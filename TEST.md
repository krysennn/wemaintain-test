# Technical test - WeMaintain

The dataset provided in the `data` folder was extracted from SongKick and has a list of bands, venues, and concerts from those bands at those venues.

- `bands.json` : List of bands (`id` and `name`)
- `venues.json` : List of places with `id`, `name`, and coordinates `latitude`/`longitude`
- `concerts.json` : list of concerts with `venueId`, `bandId` and `date` (UNIX timestamp in miliseconds)

### Notice:
- All part of the test can be acheived independantly
- Feel free to use your favorite programming language & cloud provider
- Feel free to use any API or third-party library.
- Your repository should include a `README` file that describes the steps to run your project, and contain your answers
- Your answers should take into account the time constraints that a devops team in a startup would encounter
- Answers can include benchmarks of different solutions

## Part 1: Development

The first step of the exercise is to develop a single API Endpoint that returns the concerts matching the criterias given by the user.
The endpoint should accept the four filters below as GET parameters

```
GET /concerts

bandIds: String - Comma separated list of bandIds
latitude: float
longitude: float
radius: Int - In kilometers
```

- The user must at least provide `bandIds` *OR* `latitude`/`longitude`/`radius` (But can provide all the fields too)
- Providing incorrect values should return an HTTP Bad Request code
- The endpoint must return a JSON array of matching events sorted by descending date with the following format :

```json
[
    {
        "band": "Radiohead",
        "location": "Point Ephémère, Paris, France",
        "date": 1569531810650,
        "latitude": 48.8814422,
        "longitude": 2.3684356
    },
    ...
]
```

```
{
    "id": 1,
    "name": "Band 1"
}
```

```
{
    "id": 1,
    "name": "Place 1",
    "latitude": 43.63967479999999,
    "longitude": -79.3535794
},
```

## Part 2: Architecture
For the previous step, we had 1400 bands across 376 venues, and around 20,000 events. For this step, we ask that you document in your `README.md` how you would architecture your solution if you now had 2 million bands, 10,000 venues, and 200 million events.

Describe in detail how you would store and query the data, and what kind of mechanism you would leverage to consistently deliver short response times and guarantee the highest uptime possible.

Please then answer the two following questions :

- What do you identify as possible risks in the architecture that you described in the long run, and how would you mitigate those?
- What are the key elements of your architecture that would need to be monitored, and what would you use to monitor those?


## Part 3: Infrastructure
A. How would you proceed to forbid/restrict access to the cloud production database (eg: RDS), while allowing access from team members ?

B. What kind of infrastructure would you deloy to run the BandService alongside other similar services ? What would you implement in order to guarantee the best uptime ?

C. How would you achieve a continuous deployment workflow, that would allow, upon a commit in the develop branch, to deploy new versions in staging and in production without any action from the engineering team. While guaranteeing the reliability of the production environment ?
