# Technical test - WeMaintain

## Part 1: Development

The API is written in [Go](https://golang.org/dl/) and deployed locally using Terraform on MiniKube. 
This is probably overkill for a local environment (a basic docker-compose file could have been enough) 
but is closer to what I would use to deploy in production. Please note that the Helm chart is very basic 
and must be completed to be production-ready (readiness, limits and requests of pod, ...).

### Requirements:
- Docker
    - [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
    - [MacOS](https://docs.docker.com/docker-for-mac/install/)
- [Kubernetes MiniKube cluster](https://kubernetes.io/docs/tasks/tools/install-minikube/)
- [Terraform](https://www.terraform.io/downloads.html)
- [Helm](https://helm.sh/docs/intro/install/)
- [Skaffold](https://skaffold.dev/docs/install/)

### Running:

Clone
```
$ git clone git@gitlab.com:krysennn/wemaintain-test.git
```

Prepare environment
```
$ terraform init terraform/
$ terraform apply terraform/
```

Run
```
$ skaffold dev --port-forward
```

Application is now deployed and can be used from `http://localhost:8080/concerts`

## Part 2: Architecture

**For the previous step, we had 1400 bands across 376 venues, and around 20,000 events. For this step, we ask that you document in your `README.md` how you would architecture your solution if you now had 2 million bands, 10,000 venues, and 200 million events.**

**Describe in detail how you would store and query the data, and what kind of mechanism you would leverage to consistently deliver short response times and guarantee the highest uptime possible.**

I used an [PostgreSQL](https://www.postgresql.org/) cluster in the first part of the test to store JSON data into each table.
Here a database diagram to detail the database model I used.

![](docs/diagram.png)

To query relation table I used LEFT JOIN to retrieve concerts by bands ids and max distance from latitude and longitude point. 
To calculate distance between two point I used [Haversine](https://en.wikipedia.org/wiki/Haversine_formula) formula.

**What do you identify as possible risks in the architecture that you described in the long run, and how would you mitigate those?**

In the long run, the database will be going to be slower because tables will growth and the formula to calculate distance must scan all points (latitude, longitude) of Venues table to compare with the User position given in filter.
To mitigate, I discover new interesting tool named [Apollo Federation](https://www.apollographql.com/docs/apollo-server/federation/introduction/).
I never implemented this, but it can query multiple services and aggregate results to return them in only one [graph](https://graphql.org/learn/thinking-in-graphs/).
 
With this solution, each microservice can be backed with database. Database queries are distributed around the database of each service queried to keep a good response time.

**What are the key elements of your architecture that would need to be monitored, and what would you use to monitor those?**

Golden signals about service must be `latency`, `traffic`, `errors` and `saturation`.
 
Source: https://landing.google.com/sre/sre-book/chapters/monitoring-distributed-systems/#xref_monitoring_golden-signals

About the architecture, I would monitor the CPU, RAM and DISK utilization of Kubernetes nodes exposed by the [node_exporter](https://github.com/prometheus/node_exporter), 
deployed as DaemonSet in Kubernetes cluster.
I would configure [Prometheus](https://github.com/prometheus/prometheus) jobs to scrape all metrics.
Kubernetes can exposed endpoints, nodes, pods, services and ingresses metrics through [API server](https://kubernetes.io/docs/reference/command-line-tools-reference/kube-apiserver/) and fetched from [metrics-server](https://github.com/kubernetes-sigs/metrics-server).
Database can be monitor with Prometheus too, it needed to install specific exporter or use postgres compatible cluster with built-in Prometheus metrics like [CockroachDB](https://www.cockroachlabs.com/docs/stable/monitor-cockroachdb-with-prometheus.html).

Source: https://kubernetes.io/docs/tasks/debug-application-cluster/resource-usage-monitoring/

Source: https://github.com/prometheus/prometheus/blob/master/documentation/examples/prometheus-kubernetes.yml


## Part 3: Infrastructure
**A. How would you proceed to forbid/restrict access to the cloud production database (eg: RDS), while allowing access from team members ?**

To forbid/restrict access to the [AWS RDS database](https://aws.amazon.com/fr/rds/), it's possible to using [AWS IAM](https://aws.amazon.com/fr/iam/) to manage members permissions on the database.

**B. What kind of infrastructure would you deploy to run the BandService alongside other similar services ? What would you implement in order to guarantee the best uptime ?**

I would use a managed Kubernetes cluster to host all services. Kubernetes can provide some auto-healing and auto-scaling features to have a strong resilience.

In Kubernetes cluster, i would use Istio to manage deployment of new releases with traffic shifting feature.
Istio can provide metrics, logs and tracing of application and show network communications in realtime.
- Each application must have a metrics and health endpoint to detect any outage.
- I would use GRPC to communicate with every service, especially when services are wrote in different language programming.
- Every service will be backed with a small database self-hosted or managed.

About monitoring, i would used Prometheus and Alertmanager, Prometheus can scrape Istio and Kubernetes metrics.

**C. How would you achieve a continuous deployment workflow, that would allow, upon a commit in the develop branch, to deploy new versions in staging and in production without any action from the engineering team. While guaranteeing the reliability of the production environment ?**

It's important to write a strict integration and deployment procedure, with time duration of each steps and all actions
needed to deploy each application.

1. Lint, test and build
2. Deploy in staging
- Compare metrics (cpu, ram, response time, error rate) about previous and actual version deployed, to validate the new release.
- (A benchmark with random requests can be interesting if the traffic is not enough on the staging environment).

During the staging deployment, every action does wrong must start the rollback procedure of the new release.

3. Deploy in production in multiple steps.
    - First, allowing only 15% of traffic to use the new release.
    - Second, if metrics are hopeful, switch to 35%.
    - Third, continue with 75%.
    - Finish with 100% of traffic and remove the old version of service.
    
During the production deployment, every action does wrong must start the rollback procedure of the new release.
