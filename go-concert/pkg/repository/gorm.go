package repository

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type gormRepository struct {
	DB *gorm.DB
}

// NewGormRepository will return database connection wrapped into struct
// to interact with postgres database
func NewGormRepository(uri interface{}, opts ...Options) (*gormRepository, error) {
	gormRepository := new(gormRepository)

	// open db connection
	db, err := gorm.Open("postgres", uri)
	if err != nil {
		return nil, fmt.Errorf("failed to connect to database: %v", err)
	}
	gormRepository.DB = db

	// apply options
	for _, opt := range opts {
		opt(db)
	}
	return gormRepository, nil
}

// WithPostgreSQLCredentials return PostgreSQL connection URI in GORM format.
func WithPostgreSQLCredentials(host string, port int, user string, password string, dbName string) string {
	return fmt.Sprintf(
		"host=%s port=%d user=%s dbname=%s password=%s sslmode=disable",
		host,
		port,
		user,
		dbName,
		password,
	)
}

// Options type given some custom options
// to override the default GORM database client
type Options func(db *gorm.DB)

// WithCustomLogger override default GORM Logger
func WithCustomLogger(logger gorm.Logger) func(db *gorm.DB) {
	return func(db *gorm.DB) {
		db.SetLogger(logger)
	}
}

// WithDebug enable log mode to
// log every database request
func WithDebug() func(db *gorm.DB) {
	return func(db *gorm.DB) {
		db.LogMode(true)
	}
}
