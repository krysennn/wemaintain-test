package repository

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/sirupsen/logrus"
)

func TestNewGormRepository(t *testing.T) {
	type args struct {
		uri  interface{}
		opts []Options
	}

	sql, _, _ := sqlmock.New()
	defer sql.Close()

	tests := []struct {
		name    string
		args    args
		want    *gormRepository
		wantErr bool
	}{
		{
			name: "TestWithLogMode",
			args: args{
				uri:  sql,
				opts: []Options{WithDebug()},
			},
			wantErr: false,
		},
		{
			name: "TestWithCustomLogger",
			args: args{
				uri:  sql,
				opts: []Options{WithCustomLogger(gorm.Logger{LogWriter: logrus.New()})},
			},
			wantErr: false,
		},
		{
			name: "TestWithoutOptions",
			args: args{
				uri:  sql,
				opts: nil,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			db, _ := gorm.Open("postgres", tt.args.uri)
			for _, opt := range tt.args.opts {
				opt(db)
			}
			tt.want = &gormRepository{DB: db}

			got, err := NewGormRepository(tt.args.uri, tt.args.opts...)
			if (err != nil) != tt.wantErr {
				t.Errorf("New() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("New() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestWithPostgreSQLCredentials(t *testing.T) {
	args := struct {
		host     string
		port     int
		user     string
		password string
		dbName   string
	}{
		host:     "localhost",
		port:     5432,
		user:     "foo",
		password: "bar",
		dbName:   "db-test",
	}

	expectedResult := fmt.Sprintf(
		"host=%s port=%d user=%s dbname=%s password=%s sslmode=disable",
		args.host,
		args.port,
		args.user,
		args.dbName,
		args.password,
	)

	if got := WithPostgreSQLCredentials(args.host, args.port, args.user, args.password, args.dbName); got != expectedResult {
		t.Errorf("WithPostgreSQLCredentials() = %v, want %v", got, expectedResult)
	}
}
