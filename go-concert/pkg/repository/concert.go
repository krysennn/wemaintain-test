package repository

import (
	"gitlab.com/krysennn/wemaintain-test/go-concert/pkg/model"
)

type Concert interface {
	ListByDistance(latitude, longitude float64, radius int) ([]*model.ListConcertResponse, error)
	ListByBandIds(bandIds []string) ([]*model.ListConcertResponse, error)
	List(request model.ListConcertRequest) ([]*model.ListConcertResponse, error)
}

var (
	bandIdsQuery = `SELECT c.date, b.name AS band, v.name AS location, v.latitude, v.longitude
		FROM concerts c
		LEFT JOIN bands b ON c.band_id = b.id
		LEFT JOIN venues v ON c.venue_id = v.id
		WHERE b.id IN (?)
		ORDER BY c.date DESC`

	// Distance is calculate with Haversine formula.
	// https://developers.google.com/maps/solutions/store-locator/clothing-store-locator
	// https://en.wikipedia.org/wiki/Haversine_formula
	distanceQuery = `SELECT c.date, b.name AS band, venue.latitude, venue.longitude, venue.name AS location
		FROM (SELECT id, name, latitude, longitude, haversine(?, ?, latitude, longitude) AS distance
      	FROM venues) AS venue
			LEFT JOIN concerts c ON c.venue_id = venue.id
         	LEFT JOIN bands b ON b.id = c.band_id
		WHERE distance < ?
		ORDER BY c.date DESC`
	distanceAndBandIdsQuery = `SELECT c.date, b.name AS band, venue.latitude, venue.longitude, venue.name AS location
		FROM (SELECT id, name, latitude, longitude, haversine(?, ?, latitude, longitude) AS distance
      	FROM venues) AS venue
			LEFT JOIN concerts c ON c.venue_id = venue.id
         	LEFT JOIN bands b ON b.id = c.band_id
		WHERE distance < ? AND b.id IN (?)
		ORDER BY c.date DESC`
)

func (gr *gormRepository) ListByDistance(latitude, longitude float64, radius int) ([]*model.ListConcertResponse, error) {
	var result []*model.ListConcertResponse

	err := gr.DB.Raw(distanceQuery,
		latitude,
		longitude,
		radius).
		Scan(&result).Error
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (gr *gormRepository) ListByBandIds(bandIds []string) ([]*model.ListConcertResponse, error) {
	var result []*model.ListConcertResponse

	err := gr.DB.Raw(bandIdsQuery, bandIds).
		Scan(&result).Error
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (gr *gormRepository) List(request model.ListConcertRequest) ([]*model.ListConcertResponse, error) {
	var result []*model.ListConcertResponse

	// https://developers.google.com/maps/solutions/store-locator/clothing-store-locator
	// https://en.wikipedia.org/wiki/Haversine_formula
	err := gr.DB.Raw(distanceAndBandIdsQuery,
		request.Latitude,
		request.Longitude,
		request.Radius,
		request.BandIds.List()).
		Scan(&result).Error
	if err != nil {
		return nil, err
	}
	return result, nil
}
