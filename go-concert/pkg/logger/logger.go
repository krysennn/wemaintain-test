package logger

import (
	"github.com/sirupsen/logrus"
)

// NewLogger create logrus logger with defined level and formatter
// Formatter can be Text/JSON or CustomFormatter
// Level can be TRACE,DEBUG,INFO,WARN,ERROR,FATAL or PANIC
func New(level string, format string) *logrus.Logger {
	// create logger
	logger := logrus.New()
	logger.Level, _ = logrus.ParseLevel(level)

	// define logger formatter
	switch format {
	case "TEXT":
		logger.Formatter = &logrus.TextFormatter{}
	case "JSON":
		logger.Formatter = &logrus.JSONFormatter{}
	}
	return logger
}
