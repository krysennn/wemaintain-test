package utils

import (
	"reflect"
	"regexp"
	"strings"

	"github.com/go-playground/validator/v10"
)

var (
	commaSeparatedListRegex = "^([0-9][,]?)*\\d$"
)

func NewValidator() *validator.Validate {
	validate := validator.New()
	_ = validate.RegisterValidation("commaSeparatedList", commaSeparatedListValidator)
	return validate
}

func ValidationResponse(err error) map[string]interface{} {
	response := make(map[string]interface{})
	response["message"] = "validation error"

	invalidFields := make(map[string]string)
	errs := err.(validator.ValidationErrors)
	for _, e := range errs {
		switch e.Tag() {
		case "commaSeparatedList":
			invalidFields[e.Field()] = "You must provide a comma separated list of integer !"
		case "required_without":
			response["error"] = "You must at least provide latitude, longitude and radius fields!"
			return response
		case "required_without_all":
			response["error"] = "You must at least provide bandIds OR latitude/longitude/radius (But can provide all the fields too)"
			return response
		}
	}
	response["errors"] = invalidFields
	return response
}

func commaSeparatedListValidator(fl validator.FieldLevel) bool {
	field := fl.Field()

	// check field is string
	if field.Kind() != reflect.String {
		return false
	}

	// if field is empty don't validate
	if len(strings.TrimSpace(field.String())) == 0 {
		return true
	}

	// check if field have valid list
	// valid values are:
	/// 1 OR 1,2,3
	if !regexp.MustCompile(commaSeparatedListRegex).Match([]byte(field.String())) {
		return false
	}

	// list is valid
	return true
}
