package utils

import (
	"reflect"
	"testing"
)

func Test_commaSeparatedListValidator(t *testing.T) {
	validate := NewValidator()

	tests := []struct {
		name    string
		field   string
		wantErr bool
	}{
		{
			name:    "TestWithValidList",
			field:   "1,2,3",
			wantErr: false,
		},
		{
			name:    "TestWithEmptyList",
			field:   "",
			wantErr: false,
		},
		{
			name:    "TestWithInvalidCommaInList",
			field:   ",1,2,3",
			wantErr: true,
		},
		{
			name:    "TestWithWordInList",
			field:   "a,1,2,3",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := validate.Var(tt.field, "commaSeparatedList"); (err != nil) != tt.wantErr {
				t.Errorf("commaSeparatedListValidator: error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestValidationResponse(t *testing.T) {
	validate := NewValidator()

	type ListConcertRequest struct {
		BandIds   string  `json:"bandIds,omitempty" validate:"commaSeparatedList,required_without_all=Latitude Longitude Radius"`
		Latitude  float64 `json:"latitude,omitempty" validate:"latitude,required_without=BandIds"`
		Longitude float64 `json:"longitude,omitempty" validate:"longitude,required_without=BandIds"`
		Radius    int     `json:"radius,omitempty" validate:"numeric,required_without=BandIds"`
	}

	tests := []struct {
		name string
		args ListConcertRequest
		want map[string]interface{}
	}{
		{
			name: "TestWithoutAnyFields",
			args: ListConcertRequest{},
			want: map[string]interface{}{
				"error":   "You must at least provide bandIds OR latitude/longitude/radius (But can provide all the fields too)",
				"message": "validation error",
			},
		},
		{
			name: "TestWithInvalidCommaSeparatedList",
			args: ListConcertRequest{
				BandIds: "a1,2,3",
			},
			want: map[string]interface{}{
				"errors":  map[string]string{"BandIds": "You must provide a comma separated list of integer !"},
				"message": "validation error",
			},
		},
		{
			name: "TestWithOnlyOneFieldOfGeoGroup",
			args: ListConcertRequest{
				Latitude: 1,
			},
			want: map[string]interface{}{
				"error":   "You must at least provide latitude, longitude and radius fields!",
				"message": "validation error",
			},
		},
		{
			name: "TestWithTwoFieldsOfGeoGroup",
			args: ListConcertRequest{
				Latitude: 1,
				Radius:   2,
			},
			want: map[string]interface{}{
				"error":   "You must at least provide latitude, longitude and radius fields!",
				"message": "validation error",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := validate.Struct(tt.args)
			if got := ValidationResponse(err); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ValidationResponse() = %v, want %v", got, tt.want)
			}
		})
	}
}
