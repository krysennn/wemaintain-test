package model

import (
	"reflect"
	"testing"
)

func TestListConcertRequest_Validate(t *testing.T) {
	type fields struct {
		BandIds   string
		Latitude  float64
		Longitude float64
		Radius    int
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name:    "TestWithOnlyOneBandId",
			fields:  fields{BandIds: "1"},
			wantErr: false,
		},
		{
			name:    "TestWithMultipleBandIds",
			fields:  fields{BandIds: "1,2,3"},
			wantErr: false,
		},
		{
			name: "TestWithoutBandIds",
			fields: fields{
				Latitude:  48.8814422,
				Longitude: 2.3684356,
				Radius:    5,
			},
			wantErr: false,
		},
		{
			name: "TestWithAllFields",
			fields: fields{
				BandIds:   "1,2,3",
				Latitude:  48.8814422,
				Longitude: 2.3684356,
				Radius:    5,
			},
			wantErr: false,
		},
		{
			name:    "TestWithoutFields",
			fields:  fields{},
			wantErr: true,
		},
		{
			name:    "TestWithLatitudeOnly",
			fields:  fields{Latitude: 48.8814422},
			wantErr: true,
		},
		{
			name:    "TestWithLongitudeOnly",
			fields:  fields{Longitude: 2.3684356},
			wantErr: true,
		},
		{
			name:    "TestWithRadiusOnly",
			fields:  fields{Radius: 5},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			lcr := &ListConcertRequest{
				BandIds:   BandIds(tt.fields.BandIds),
				Latitude:  tt.fields.Latitude,
				Longitude: tt.fields.Longitude,
				Radius:    tt.fields.Radius,
			}
			if err := lcr.Validate(); (err != nil) != tt.wantErr {
				t.Errorf("ListConcertRequest.Validate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestBandIds_List(t *testing.T) {
	tests := []struct {
		name string
		b    BandIds
		want []string
	}{
		{
			name: "TestWithOneID",
			b:    BandIds("1"),
			want: []string{"1"},
		},
		{
			name: "TestWithListOfIDCommaSeparated",
			b:    BandIds("1,2,3"),
			want: []string{"1", "2", "3"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.b.List(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("BandIds.List() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBandIds_String(t *testing.T) {
	tests := []struct {
		name string
		b    BandIds
		want string
	}{
		{
			name: "TestWithOneID",
			b:    BandIds("1"),
			want: "1",
		},
		{
			name: "TestWithListOfIDCommaSeparated",
			b:    BandIds("1,2,3"),
			want: "1,2,3",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.b.String(); got != tt.want {
				t.Errorf("BandIds.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestListConcertRequest_Location(t *testing.T) {
	latitudeExpected := 48.729751799999995
	longitudeExpected := 48.729751799999995
	radiusExpected := 20

	lcr := &ListConcertRequest{
		BandIds:   "1,2,3",
		Latitude:  latitudeExpected,
		Longitude: longitudeExpected,
		Radius:    radiusExpected,
	}

	got, got1, got2 := lcr.Location()
	if got != latitudeExpected {
		t.Errorf("ListConcertRequest.Location() got = %v, want %v", got, latitudeExpected)
	}
	if got1 != longitudeExpected {
		t.Errorf("ListConcertRequest.Location() got1 = %v, want %v", got1, longitudeExpected)
	}
	if got2 != radiusExpected {
		t.Errorf("ListConcertRequest.Location() got2 = %v, want %v", got2, radiusExpected)
	}
}
