package model

import (
	"strings"

	"gitlab.com/krysennn/wemaintain-test/go-concert/pkg/utils"
)

// ListConcertRequest user input filters
type ListConcertRequest struct {
	BandIds   BandIds `json:"bandIds,omitempty" validate:"commaSeparatedList,required_without_all=Latitude Longitude Radius"`
	Latitude  float64 `json:"latitude,omitempty" validate:"latitude,required_without=BandIds"`
	Longitude float64 `json:"longitude,omitempty" validate:"longitude,required_without=BandIds"`
	Radius    int     `json:"radius,omitempty" validate:"numeric,required_without=BandIds"`
}

// ListConcertResponse response structure
type ListConcertResponse struct {
	Band      string  `json:"band"`
	Location  string  `json:"location"`
	Date      int64   `json:"date"`
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

type BandIds string

// List return array of id instead of
// comma-separated list string
func (b BandIds) List() []string {
	return strings.Split(string(b), ",")
}

// String return string instead of BandIds type
func (b BandIds) String() string {
	return string(b)
}

// Location is shortcut function to return latitude, longitude
// and radius from ListConcertRequest instead of complete struct
func (lcr *ListConcertRequest) Location() (float64, float64, int) {
	return lcr.Latitude, lcr.Longitude, lcr.Radius
}

// Validate ListConcertRequest struct with validate
// field set on each field.
func (lcr *ListConcertRequest) Validate() error {
	validator := utils.NewValidator()
	err := validator.Struct(lcr)
	if err != nil {
		return err
	}
	return nil
}
