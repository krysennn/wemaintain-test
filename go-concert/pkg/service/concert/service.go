package concert

import (
	"github.com/sirupsen/logrus"

	"gitlab.com/krysennn/wemaintain-test/go-concert/pkg/model"
	"gitlab.com/krysennn/wemaintain-test/go-concert/pkg/repository"
)

type Service interface {
	List(request model.ListConcertRequest) ([]*model.ListConcertResponse, error)
}

type Concert struct {
	repository repository.Concert
	logger     *logrus.Logger
}

func NewService(db repository.Concert, logger *logrus.Logger) *Concert {
	return &Concert{
		repository: db,
		logger:     logger,
	}
}

func (s *Concert) List(request model.ListConcertRequest) ([]*model.ListConcertResponse, error) {
	// list concerts by bandIds
	if request.BandIds != "" &&
		request.Latitude == 0 &&
		request.Longitude == 0 {
		concerts, err := s.repository.ListByBandIds(request.BandIds.List())
		if err != nil {
			return nil, err
		}
		return concerts, nil
	}

	// list concerts by distance
	if request.BandIds == "" {
		concerts, err := s.repository.ListByDistance(request.Location())
		if err != nil {
			return nil, err
		}
		return concerts, nil
	}

	// list concert by bandIds and distance
	concerts, err := s.repository.List(request)
	if err != nil {
		return nil, err
	}
	return concerts, nil
}
