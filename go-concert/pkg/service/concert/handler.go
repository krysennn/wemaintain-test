package concert

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/krysennn/wemaintain-test/go-concert/pkg/model"
	"gitlab.com/krysennn/wemaintain-test/go-concert/pkg/utils"
)

func ListConcert(service *Concert) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// close body in end of function
		defer r.Body.Close()

		// apply correct content-type
		w.Header().Set("Content-Type", "application/json")

		// get url params
		params := r.URL.Query()
		bandIds := params.Get("bandIds")
		latitude, _ := strconv.ParseFloat(params.Get("latitude"), 64)
		longitude, _ := strconv.ParseFloat(params.Get("longitude"), 64)
		radius, _ := strconv.Atoi(params.Get("radius"))

		// fill struct request with params
		reqModel := model.ListConcertRequest{
			BandIds:   model.BandIds(bandIds),
			Latitude:  latitude,
			Longitude: longitude,
			Radius:    radius,
		}

		// validate
		err := reqModel.Validate()
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			data, _ := json.Marshal(utils.ValidationResponse(err))
			_, _ = w.Write([]byte(data))
			return
		}

		// query service to retrieve list of concerts
		// match with filters
		concerts, err := service.List(reqModel)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		responseData, err := json.Marshal(concerts)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(responseData)
	}
}
