package main

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"gopkg.in/alecthomas/kingpin.v2"

	"gitlab.com/krysennn/wemaintain-test/go-concert/pkg/logger"
	"gitlab.com/krysennn/wemaintain-test/go-concert/pkg/repository"
	"gitlab.com/krysennn/wemaintain-test/go-concert/pkg/service/concert"
)

var (
	version string

	listenAddr = kingpin.Flag("web.listenAddr", "Address on which to expose http server.").
		Default(":8080").
		String()
	logLevel = kingpin.Flag("log.level", "Define log level").
		Default("INFO").
		Enum("DEBUG", "INFO", "WARN", "ERROR")
	logFormat = kingpin.Flag("log.format", "Define log format").
		Default("TEXT").
		Enum("JSON", "TEXT")
	dbHost = kingpin.Flag("db.host", "Host or database server").
		Required().
		Envar("GO_CONCERT_DB_HOST").
		String()
	dbPort = kingpin.Flag("db.port", "Port of database server").
		Default("5432").
		Envar("GO_CONCERT_DB_PORT").
		Int()
	dbName = kingpin.Flag("db.name", "Name of database you want use").
		Required().
		Envar("GO_CONCERT_DB_NAME").
		String()
	dbUsername = kingpin.Flag("db.username", "Database username").
		Required().
		Envar("GO_CONCERT_DB_USERNAME").
		String()
	dbPassword = kingpin.Flag("db.password", "Database password").
		Required().
		Envar("GO_CONCERT_DB_PASSWORD").
		String()
	dbMigration = kingpin.Flag("db.migration", "running migration at runtime").
		Default("true").
		Envar("GO_CONCERT_DB_MIGRATION").
		Bool()
)

func main() {
	kingpin.Parse()
	kingpin.Version(version)

	// init logger
	log := logger.New(*logLevel, *logFormat)

	// init repository options
	repositoryOptions := []repository.Options{
		repository.WithCustomLogger(gorm.Logger{LogWriter: log}),
	}
	if *logLevel == "DEBUG" {
		repositoryOptions = append(repositoryOptions, repository.WithDebug())
	}

	// init repository
	repo, err := repository.NewGormRepository(
		repository.WithPostgreSQLCredentials(*dbHost, *dbPort, *dbUsername, *dbPassword, *dbName),
		repositoryOptions...,
	)
	if err != nil {
		log.WithError(err).Fatal("failed to connect to database")
	}
	defer repo.DB.Close()

	// database migration
	if *dbMigration {
		log.Infof("DB: running migrations ...")
		err = repo.Migrate()
		if err != nil {
			log.WithError(err).Fatal("failed to run database migrations")
		}
		log.Infof("DB: migrations executed: database is up to date")
	}

	// init service
	concertSvc := concert.NewService(repo, log)

	// init router and register handlers
	router := mux.NewRouter()
	router.HandleFunc("/concerts", concert.ListConcert(concertSvc)).Methods("GET")
	router.HandleFunc("/healthz", func(writer http.ResponseWriter, request *http.Request) {
		writer.WriteHeader(200)
	})

	log.Infof("Listen: %v", *listenAddr)
	err = http.ListenAndServe(*listenAddr, router)
	if err != nil {
		log.Fatal(err)
	}
}
