module gitlab.com/krysennn/wemaintain-test/go-concert

go 1.13

require (
	github.com/DATA-DOG/go-sqlmock v1.4.1
	github.com/go-playground/assert/v2 v2.0.1
	github.com/go-playground/validator/v10 v10.2.0
	github.com/golang-migrate/migrate/v4 v4.8.0
	github.com/gorilla/mux v1.7.3
	github.com/jinzhu/gorm v1.9.12
	github.com/lib/pq v1.3.0
	github.com/prometheus/client_golang v1.4.1
	github.com/sirupsen/logrus v1.4.2
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
