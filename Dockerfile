FROM golang:1.13 AS builder

ARG VERSION

WORKDIR /go/src/gitlab.com/krysennn/wemaintain-test/go-concert

COPY go-concert .

RUN CGO_ENABLED=0 GOOS=linux go build -ldflags="-X main.version=${VERSION}" cmd/api.go
RUN pwd
RUN ls -la

FROM busybox:glibc

LABEL MAINTAINER camilleri.alexis@gmail.com
LABEL description = "api go-concert"

COPY --from=builder /go/src/gitlab.com/krysennn/wemaintain-test/go-concert/api /bin/go-concert-api
COPY go-concert/migrations /go-concert/migrations

EXPOSE      8080

ENTRYPOINT  [ "/bin/go-concert-api" ]
