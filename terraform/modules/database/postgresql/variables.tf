variable "name" {}

variable "chart_version" {
  type = string
  default = "8.3.0"
}

variable "database_name" {
  type = string
  default = "postgres"
}

variable "username" {
  type = string
  default = "postgres"
}

variable "password" {
  type = string
}
