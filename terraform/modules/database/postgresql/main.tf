data "helm_repository" "stable" {
  name = "stable"
  url = "https://kubernetes-charts.storage.googleapis.com"
}

resource "helm_release" "postgresql" {
  name = var.name
  repository = data.helm_repository.stable.metadata[0].name
  chart = "postgresql"
  version = var.chart_version

  set {
    name = "postgresqlDatabase"
    value = var.database_name
  }

  set {
    name = "postgresqlUsername"
    value = var.username
  }

  set {
    name = "postgresqlPassword"
    value = var.password
  }
}

resource "kubernetes_secret" "postgres_credentials" {
  metadata {
    name = "postgres-credentials"
  }

  data = {
    username = var.username
    password = var.password
  }

  depends_on = [
    helm_release.postgresql
  ]
}
