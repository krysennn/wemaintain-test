locals {
  context = {
    kubernetes_config_path = pathexpand(var.config_path)
    kubernetes_context = var.config_context
  }
}

provider kubernetes {
  config_path = local.context.kubernetes_config_path
  config_context = local.context.kubernetes_context
}

provider "helm" {
  kubernetes {
    config_path = local.context.kubernetes_config_path
    config_context = local.context.kubernetes_context
  }
}

module "database" {
  source = "./modules/database/postgresql"
  name = "postgresql"

  database_name = "test-db"
  // in production: use with environment variable
  username = "admin"
  password = "password"
}
